#include <gtk/gtk.h>
#include <gst/gst.h>
#include <gst/video/videooverlay.h>

#ifdef GDK_WINDOWING_WAYLAND
#include <gdk/gdkwayland.h>
#else
#error "Wayland is not supported in GTK+"
#endif

#include "wayland.h"
#include "gresource-multistream-player.h"

#define MSS_SERVER "192.168.1.237"
#define STREAM_WIDTH 1280
#define STREAM_HEIGHT 720
#define VIDEO_CAPS "video/x-raw,format=RGB16,width=" \
  G_STRINGIFY (STREAM_WIDTH) ",height=" G_STRINGIFY (STREAM_HEIGHT) \
  ";video/x-raw,width=" \
  G_STRINGIFY (STREAM_WIDTH) ",height=" G_STRINGIFY (STREAM_HEIGHT)
#define VIDEO_SINK "videoconvert ! videoscale ! " VIDEO_CAPS " ! waylandsink"
#ifdef __arm__
#define HW_DECODER "v4l2h264dec"
#else
#warning "Not building with RPi4 HW decoder"
#define HW_DECODER "decodebin"
#endif


typedef struct {
  const gchar *description;
  gint x;
  gint y;
  GstElement *pipeline;
} MssStream;

typedef struct {
  GApplication *app;
  GtkWidget *window;
} MssPlayer;

static MssPlayer player = { NULL, };
static MssStream streams_data[] = {
  { "srtsrc uri=srt://" MSS_SERVER ":7002 latency=50 do-timestamp=1 "
      "! tsdemux latency=50 ! h264parse ! avdec_h264 ! "
        VIDEO_SINK, 60, 60, NULL },
  { "ristsrc address=224.0.0.1 port=5004 receiver-buffer=50 "
    "! rtpmp2tdepay ! tsdemux latency=50 ! h264parse ! avdec_h264 ! "
      VIDEO_SINK, 60, 1242, NULL},
  { "souphttpsrc location=http://" MSS_SERVER ":8080/hls/playlist.m3u8 ! hlsdemux "
    "! tsdemux ! h264parse ! " HW_DECODER " ! "
      VIDEO_SINK, 2500,1242, NULL },
};

static gboolean setup_stream (MssStream *stream);


static void
validate_system (void)
{
  GdkDisplay *display;
  GdkWindow *window;
  GdkMonitor *monitor;
  GdkRectangle geometry;

  display = gtk_widget_get_display (player.window);
  window = gtk_widget_get_window (player.window);
  monitor = gdk_display_get_monitor_at_window (display, window);
  gdk_monitor_get_geometry (monitor, &geometry);

  if (geometry.width != 3840 || geometry.height != 2160) {
    GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (player.window),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
        "This demo is designed for 4K display.");
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_hide (dialog);
  }

  if (!GDK_IS_WAYLAND_DISPLAY (display)) {
    GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (player.window),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
        "This demo only works with on Wayland.");
    gtk_dialog_run (GTK_DIALOG (dialog));
    g_application_quit (player.app);
  }
}

static gboolean
play_stream (MssStream *stream)
{
  gst_element_set_state (stream->pipeline, GST_STATE_PLAYING);
  return G_SOURCE_REMOVE;
}

static void
error_cb (GstBus * bus, GstMessage * msg, gpointer user_data)
{
  MssStream *stream = user_data;
  g_autofree gchar *debug = NULL;
  g_autoptr (GError) err = NULL;

  gst_message_parse_error (msg, &err, &debug);

  g_print ("Error: %s\n", err->message);
  if (debug)
    g_print ("Debug details: %s\n", debug);

  gst_element_set_state (stream->pipeline, GST_STATE_NULL);
  g_clear_object (&stream->pipeline);

  /* Retry in 3s...*/
  g_timeout_add_seconds (3, (GSourceFunc) setup_stream, stream);
}

static GstBusSyncReply
bus_sync_handler (GstBus * bus, GstMessage * message, gpointer user_data)
{
  MssStream *stream = user_data;

  if (gst_is_wayland_display_handle_need_context_message (message)) {
    GstContext *context;
    GdkDisplay *display;
    struct wl_display *display_handle;

    display = gtk_widget_get_display (player.window);
    display_handle = gdk_wayland_display_get_wl_display (display);
    context = gst_wayland_display_handle_context_new (display_handle);
    gst_element_set_context (GST_ELEMENT (GST_MESSAGE_SRC (message)), context);

    goto drop;
  } else if (gst_is_video_overlay_prepare_window_handle_message (message)) {
    GstVideoOverlay *overlay;
    GdkWindow *window;
    struct wl_surface *window_handle;

    overlay = GST_VIDEO_OVERLAY (GST_MESSAGE_SRC (message));
    window = gtk_widget_get_window (player.window);
    window_handle = gdk_wayland_window_get_wl_surface (window);

    gst_video_overlay_set_window_handle (overlay, (guintptr) window_handle);
    gst_video_overlay_set_render_rectangle (overlay, stream->x, stream->y,
        STREAM_WIDTH, STREAM_HEIGHT);

    goto drop;
  }

  return GST_BUS_PASS;

drop:
  gst_message_unref (message);
  return GST_BUS_DROP;
}

static gboolean
setup_stream (MssStream *stream)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GstBus) bus = NULL;

  stream->pipeline = gst_parse_launch (stream->description, &error);
  if (!stream->pipeline) {
    GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (player.window),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
        "Failed to create stream pipeline: %s", error->message);
    gtk_dialog_run (GTK_DIALOG (dialog));
    g_application_quit (player.app);
    return G_SOURCE_REMOVE;
  }

  bus = gst_pipeline_get_bus (GST_PIPELINE (stream->pipeline));
  gst_bus_add_signal_watch (bus);
  g_signal_connect (bus, "message::error", G_CALLBACK (error_cb), stream);
  gst_bus_set_sync_handler (bus, bus_sync_handler, stream, NULL);


  play_stream (stream);

  return G_SOURCE_REMOVE;
}

static void
setup_streams ()
{
  gint i;

  for (i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];

    setup_stream (stream);
  }
}

static void
stop_streams ()
{
  gint i;
  for (i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];
    if (stream->pipeline) {
      gst_element_set_state (stream->pipeline, GST_STATE_NULL);
      g_clear_object (&stream->pipeline);
    }
  }
}

static void
activate_cb (GApplication * app, gpointer user_data)
{
  GtkWidget *background;

  player.window = gtk_application_window_new (GTK_APPLICATION (app));
  gtk_window_set_title (GTK_WINDOW (player.window), "Multi-Stream Player");
  gtk_window_set_hide_titlebar_when_maximized (GTK_WINDOW (player.window), TRUE);
  gtk_window_set_resizable (GTK_WINDOW (player.window), FALSE);
  gtk_widget_show (player.window);

  validate_system ();

  background = gtk_image_new_from_resource ("/background.svg");
  gtk_container_add (GTK_CONTAINER (player.window), background);
  gtk_widget_show (background);
  gtk_window_fullscreen (GTK_WINDOW (player.window));

  setup_streams ();
}

gint
main (gint argc, gchar **argv)
{
  GError *error = NULL;
  GOptionContext *context;
  GOptionEntry entries[] = {
    { NULL }
  };
  g_autoptr (GtkApplication) app;
  gint status;

  context = g_option_context_new ("- multistream player");
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_add_group (context, gst_init_get_option_group ());
  g_option_context_add_group (context, gtk_get_option_group (TRUE));
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_print ("option parsing failed: %s\n", error->message);
    exit (1);
  }

  g_resources_register (multistream_player_get_resource());

  app = gtk_application_new ("com.collabora.multistream-player",
      G_APPLICATION_FLAGS_NONE);
  player.app = G_APPLICATION (app);
  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  stop_streams ();

  return status;
}
